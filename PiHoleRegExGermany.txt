r[0-9-]-sn-[a-z0-9]-[0-9a-z]{4}.googlevideo.com	
^r[0-9]-*.*.googlevideo.com 
^(.+[_.-])?eulerian\.net$ 
^(.+[_.-])?dnsdelegation\.io$ 
^(www[0-9]*\.)?xn-- 
(^r)+([0-9])\-+([a-z]+)\-([a-z,a-z,0-9]+\.([a-z])+\.([a-z])+) 
^ad([sxv]?[0-9]*|system)[_.-]([^.[:space:]]+\.){1,}|[_.-]ad([sxv]?[0-9]*|system)[_.-] 
^(.+[_.-])?adse?rv(er?|ice)?s?[0-9]*[_.-] 
^(.+[_.-])?telemetry[_.-] 
^adim(age|g)s?[0-9]*[_.-] 
^adtrack(er|ing)?[0-9]*[_.-] 
^advert(s|is(ing|ements?))?[0-9]*[_.-] 
^aff(iliat(es?|ion))?[_.-] 
^analytics?[_.-] 
^banners?[_.-] 
^beacons?[0-9]*[_.-] 
^count(ers?)?[0-9]*[_.-] 
^mads\. 
^pixels?[-.] 
^https?://([A-Za-z0-9.-]*\.)?clicks\.beap\.bc\.yahoo\.com/ 
^https?://([A-Za-z0-9.-]*\.)?secure\.footprint\.net/ 
^https?://([A-Za-z0-9.-]*\.)?match\.com/ 
^https?://([A-Za-z0-9.-]*\.)?clicks\.beap\.bc\.yahoo(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?sitescout(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?appnexus(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?evidon(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?mediamath(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?scorecardresearch(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?doubleclick(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?flashtalking(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?turn(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?mathtag(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?googlesyndication(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?s\.yimg\.com/cv/ae/us/audience/ 
^https?://([A-Za-z0-9.-]*\.)?clicks\.beap/ 
^https?://([A-Za-z0-9.-]*\.)?.doubleclick(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?yieldmanager(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?w55c(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?adnxs(\.\w{2}\.\w{2}|\.\w{2,4})/ 
^https?://([A-Za-z0-9.-]*\.)?advertising\.com/ 
^https?://([A-Za-z0-9.-]*\.)?evidon\.com/ 
^https?://([A-Za-z0-9.-]*\.)?scorecardresearch\.com/ 
^https?://([A-Za-z0-9.-]*\.)?flashtalking\.com/ 
^https?://([A-Za-z0-9.-]*\.)?turn\.com/ 
^https?://([A-Za-z0-9.-]*\.)?mathtag\.com/ 
^https?://([A-Za-z0-9.-]*\.)?surveylink/ 
^https?://([A-Za-z0-9.-]*\.)?info\.yahoo\.com/ 
^https?://([A-Za-z0-9.-]*\.)?ads\.yahoo\.com/ 
^https?://([A-Za-z0-9.-]*\.)?global\.ard\.yahoo\.com/ 
^(.+[-_.])??adse?rv(er?|ice)?s?[0-9]*[-.] 
^(.+[-_.])??m?ad[sxv]?[0-9]*[-_.] 
^adim(age|g)s?[0-9]*[-_.] 
^adtrack(er|ing)?[0-9]*[-.] 
^advert(s|is(ing|ements?))?[0-9]*[-_.] 
^aff(iliat(es?|ion))?[-.] 
^analytics?[-.] 
^banners?[-.] 
^beacons?[0-9]*[-.] 
^count(ers?)?[0-9]*[-.] 
^hpopenbid?[-.] 
^outbrain?[-.] 
^prebid?[-.] 
^revcontent?[-.] 
^rubiconproject?[-.] 
^taboola?[-.] 
^telemetry[-.] 
^traff(ic)?[-.] 
(^|\.)ibs\.lgappstv\.com 
(^|\.)lgsmartad\.com 
(^|\.)smartshare\.lgtvsdp\.com 
(^|\.)rdx2\.lgtvsdp\.com 
^[a-z]{11,15}$ 
(^|\.)giraffic\.com$ 
(^|\.)internetat\.tv$ 
(^|\.)pavv\.co\.kr$ 
(^|\.)samsungcloudcdn\.com$ 
(^|\.)samsungcloudsolution\.com$ 
(^|\.)samsungcloudsolution\.net$ 
(^|\.)samsungelectronics\.com$ 
(^|\.)samsungotn\.net$ 
(^|\.)samsungrm\.net$ 
(\.|^)tvinteractive\.tv$ 
(^|\.)myhomescreen\.tv$ 
^api\..*\.hismarttv\.com$ 
r[0-9\-]*\-sn\-[a-z0-9]*\-[0-9a-z]{4}\.googlevideo\.com 
^(.+[_.-])?ad[sxv]?[0-9]*[_.-] 
(googlesyndication\.com)$ 
(1stok\.com)$ 
(strip\.alicdn\.com)$ 
(247realmedia\.com)$ 
(2cnt\.net)$ 
(2mdn\.net)$ 
(2o7\.net)$ 
(302br\.net)$ 
(51yes\.com)$ 
(about-tabs\.com)$ 
(adalyser\.com)$ 
(adbrite\.com)$ 
(adbureau\.net)$ 
(addthis\.com)$ 
(adk2\.co)$ 
(admob\.com)$ 
(adnxs\.com)$ 
(adocean\.pl)$ 
(adprotect\.net)$ 
(adsafeprotected\.com)$ 
(adscience\.nl)$ 
(adserver\.com)$ 
(adtech\.de)$ 
(adtech\.fr)$ 
(adtech\.us)$ 
(adtlgc\.com)$ 
(advance\.net)$ 
(advertising\.com)$ 
(advertserve\.com)$ 
(am15\.net)$ 
(amazon-adsystem\.com)$ 
(appier\.net)$ 
(applovin\.com)$ 
(atdmt\.com)$ 
(bannerbank\.ru)$ 
(bbelements\.com)$ 
(bravenet\.com)$ 
(casalemedia\.com)$ 
(cedexis-radar\.net)$ 
(checkm8\.com)$ 
(cjt1\.net)$ 
(cnzz\.com)$ 
(cqcounter\.com)$ 
(crashlytics\.com)$ 
(crypto-loot\.com)$ 
(doubleclick\.be)$ 
(doubleclick\.net)$ 
(dynamic\.dol\.ru)$ 
(economicoutlook\.net)$ 
(esomniture\.com)$ 
(estat\.com)$ 
(extreme-dm\.com)$ 
(ezcybersearch\.com)$ 
(falkag\.net)$ 
(fastclick\.net)$ 
(focalink\.com)$ 
(gemius\.pl)$ 
(geovisite\.com)$ 
(glam\.com)$ 
(glpals\.com)$ 
(googleadservices\.com)$ 
(google-analytics\.com)$ 
(googlezip\.net)$ 
(groovinads\.com)$ 
(hitbox\.com)$ 
(hotlog\.ru)$ 
(hpg\.com\.br)$ 
(hpg\.ig\.com\.br)$ 
(hut1\.ru)$ 
(hyperbanner\.net)$ 
(imrworldwide\.com)$ 
(intellitxt\.com)$ 
(iovation\.com)$ 
(kaffnet\.com)$ 
(kontera\.com)$ 
(linkpulse\.com)$ 
(liteweb\.net)$ 
(liveadvert\.com)$ 
(marketgid\.com)$ 
(marketscore\.com)$ 
(mydas\.mobi)$ 
(oewabox\.at)$ 
(omniture\.com)$ 
(omtrdc\.net)$ 
(opentracker\.net)$ 
(ourtablets\.com)$ 
(p2l\.info)$ 
(paycount\.com)$ 
(petrovka\.info)$ 
(polybuild\.ru)$ 
(pop6\.com)$ 
(popmarker\.com)$ 
(popupad\.net)$ 
(pos\.baidu\.com)$
(sapo\.pt)$ 
(servedbyopenx\.com)$ 
(sextracker\.be)$ 
(sextracker\.com)$ 
(sextracker\.de)$ 
(shengen\.ru)$ 
(sitemeter\.com)$ 
(smartadserver\.com)$ 
(spylog\.com)$ 
(startappexchange\.com)$ 
(startappnetwork\.com)$ 
(startappservice\.com)$ 
(statcounter\.com)$ 
(surf-town\.net)$ 
(thruport\.com)$ 
(tradedoubler\.com)$ 
(tsyndicate\.com)$ 
(tynt\.com)$ 
(vkuservideo\.net)$ 
(voluumtrk\.com)$ 
(voodoo\.com)$ 
(web3000\.com)$ 
(webtrekk\.net)$ 
(xiti\.com)$ 
(xxxcounter\.com)$ 
(youku\.com)$ 
(zedo\.com)$ 
^(ad\.) 
^(ad1\.) 
^(ad2\.) 
^(ad3\.) 
^(adimages\.) 
^(ads\.) 
^(ads1\.) 
^(ads2\.) 
^(ads3\.) 
^(ads4\.) 
^(ads5\.) 
^(adserv\.) 
^(adserve\.) 
^(adserver\.) 
^(adserver1\.) 
^(adserver2\.) 
^(adsrv\.) 
^(adv\.) 
^(advertising\.) 
^(adx\.) 
^(aff\.) 
^(affiliate\.) 
^(affiliates\.) 
^(analytics\.) 
^(banner\.) 
^(banners\.) 
^(clicks\.) 
^(counter\.) 
^(gcirm\.) 
^(oas\.) 
^(oascentral\.) 
^(phorm\.) 
^(pixel\.) 
^(protection\.) 
^(reklam\.) 
^(scrooge\.) 
^(stat\.) 
^(track\.) 
^(tracker\.) 
^(tracking\.) 
(^|\.)pokolorujfb.*\.glt\.pl$ 
(^|\.)agloclar\.com$ 
(^|\.)allegroloklinie-oferta.*\.xyz$ 
(^|\.)allegro(finanse)?[0-9]+\.pl$ 
(^|\.)allegro\.pl-nowe-regulamin.*\.*\.com$ 
(^|\.)allegro\.pl\..*\.pl$ 
(^|\.)apple-.*\.deviceoffergiveaways\.icu$ 
(^|\.)bestshopping-voucher\.com$ 
(^|\.)cache\.server560960\.nazwa\.pl$ 
(^|\.)carhourshall\.top$ 
(^|\.)creativego[a-z0-9]{20}\.com$ 
(^|\.)dates-here-now.*\.com$ 
(^|\.)dating-4-adults.*\.com$ 
(^|\.)dating-heart.*\.com$ 
(^|\.)distracted-babbage-.*\.netlify\.app$ 
(^|\.)dobrapraca.*\.ct8\.pl$ 
(^|\.)dreamwoman-finder.*\.com$ 
(^|\.)faccebook.*\.5v\.pl$ 
(^|\.)facebbook.*\.5v\.pl$ 
(^|\.)facebook.*\.5v\.pl$ 
(^|\.)facebook.*\.7m\.pl$ 
(^|\.)faktury[0-9]+\.org$ 
(^|\.)faktypolska.*\.b-cdn\.net$ 
(^|\.)fakt.*\.is-best\.net$ 
(^|\.)fb-service-login.*\.you2\.pl$ 
(^|\.)findyourlovenow.*\.com$ 
(^|\.)google-.*\.*giveaway*\.xyz$ 
(^|\.)google-.*\.*offer*\.icu$ 
(^|\.)google-.*\.*present*\.icu$ 
(^|\.)historieludzkie.*\.online$ 
(^|\.)inform.*\.is-best\.net$ 
(^|\.)instasexlocator.*\.com$ 
(^|\.)kartliew\.com$ 
(^|\.)komornik.*-gov\.tk$ 
(^|\.)lifehack[0-9]+\.wikmostwantedxc\.pl$ 
(^|\.)localdates.*\.com$ 
(^|\.)lokalneinformacje.*\.vot\.pl$ 
(^|\.)mobile-app-market-here.*\.life$ 
(^|\.)money-for-you-.*\.ru$ 
(^|\.)mysexdating.*\.club$ 
(^|\.)newsy.*\.pro-linuxpl\.com$ 
(^|\.)nvisionpropl.*\.com$ 
(^|\.)platnik.*\.online$ 
(^|\.)pl\..*bitcoin.*\.tiptopko.*\.com$ 
(^|\.)polityka[0-9]+\.mobiklivestmox\.pl$ 
(^|\.)polskapolicja24\.pl\..*\..*\.cz$ 
(^|\.)polska-teraz.*\.idl\.pl$ 
(^|\.)poszukiwania.*\.com\.pl$ 
(^|\.)procars-shoppl.*\.com$ 
(^|\.)pulsoksymetr.*pl\.com$ 
(^|\.)randkiero\.date$ 
(^|\.)reasonoppositewash\.top$ 
(^|\.)riddexpl.*\.com$ 
(^|\.)sharefoto-.*\.idl\.pl$ 
(^|\.)sharefoto-.*\.vot\.pl$ 
(^|\.)shop.*\.suaposales\.com$ 
(^|\.)shop[0-9]+\.onlinediscount2021\.ru$ 
(^|\.)shop[0-9]+\.onlineshops2021\.ru$ 
(^|\.)skillmodernplan\.top$ 
(^|\.)sprnr.*\.net$ 
(^|\.)superchance-forwin.*\.life$ 
(^|\.)thousandquotientscience\.top$ 
(^|\.)tvp-informacyjna.*\.eu$ 
(^|\.)update\..*sf*\.online$ 
(^|\.)wp\.[0-9]+\.top$ 
(^|\.)your-mac-security-analysis\.net\..*\.*\.agency$ 
(^|\.)your-mac-security-analysis\.net\..*\.*\.icu$ 
(^|\.)your-mac-security-analysis\.net\..*\.*\.life$ 
(^|\.)your-mac-security-analysis\.net\..*\.*\.live$ 
(^|\.)your-mac-security-analysis\.net\..*\.*\.xyz$ 
(^|\.)youtube\.com\.[0-9a-z-]+\.[0-9a-z-]+\.com$ 
(^|\.)zaplata.*\.tk$ 
(^|\.)zdjecie-facebook-.*\.dkonto\.pl$ 
(^|\.)(info-onet|informujemy)[0-9]+?\.pl$ 
(^|\.)(inwestpoland|inwestpl|polandinw|lotos-poland|poland-lotos)[a-z]+?[0-9]+\.(site|space)$ 
(^|\.)(probablerootport)-?[0-9]+\.live$ 
(^|\.).*automatyczny-dozownikmydla-pl\.com$ 
(^|\.).*automatyczny-dozownik-mydla-pl\.com$ 
(^|\.).*dworzeconline24\.*$ 
(^|\.).*dworzecporwaniepl*\.*$ 
(^|\.).*dworzecporwaniepl\.*$ 
(^|\.).*dziewczynkaporwana\.*$ 
(^|\.).*iporwania24\.*$ 
(^|\.).*iporwania-24\.pl$ 
(^|\.).*iporwania\.*$ 
(^|\.).*iporwanie24\.*$ 
(^|\.).*iporwanie\.*$ 
(^|\.).*polskaporwanie\.*$ 
(^|\.).*porwaniapolska24*\.*$ 
(^|\.).*porwaniapolska48*\.*$ 
(^|\.).*porwaniapolska\.*$ 
(^|\.).*porwaniedziecka\.*$ 
(^|\.).*porwaniepolska24*\.*$ 
(^|\.).*porwaniepolska48*\.*$ 
(^|\.).*porwaniepolska\.*$ 
(^|\.).*poszukiwanadziewczynka\.*$ 
(^|\.).*poszukiwananatalka\.*$ 
(^|\.).*poszukiwanialaury\.*$ 
(^|\.).*uprowadzeniedworzec\.*$ 
(^|\.).*uprowadzniezdworca\.*$ 
(^|\.).*-?emonitoring-?e?poczta.*$ 
(^|\.).*-?(fakty?|news|wiadomosci?)gwalt[0-9]+?\..*$ 
(^|\.).*\.*\.mirsolar\.com\.tr$ 
(^|\.)?.?[a-z]+-?polska[0-9]+?-?poczta\..*$ 
(^|\.)[0-9]+platnosci?\.online$ 
(^|\.)[0-9]+\.adwokat-mk\.pl$ 
(^|\.)[a-z]+-?poczta(\d+)?-?polska\..*$ 
(^|\.)[a-z]+-?pocztex[0-9]+-?(polska)?\..*$ 
(^|\.)[a-z]+?-?pocztex.*[0-9]+\..*$ 
(^|\.)[a-z]+?-?[0-9]+?gwalt(fakty?|news|wiadomosci?|polska|monitoring)\..*$ 
(^|\.)[a-z]+?\.?poczta-?polska[0-9]+\..*$ 
(^|\.)barsy.*\.blogspot\.com$ 
(^|\.)czas-na-fb.*\.blogspot\.com$ 
(^|\.)fabryka-chinczykow.*\.blogspot\.com$ 
(^|\.)facebokznajomi.*\.blogspot\.com$ 
(^|\.)facebook-kolor.*\.blogspot\.com$ 
(^|\.)fbcolor.*\.blogspot\.com$ 
(^|\.)fbpodglad.*\.blogspot\.com$ 
(^|\.)fb-podglad.*\.blogspot\.com$ 
(^|\.)fcolors.*\.blogspot\.com$ 
(^|\.)fejsopodgladacz.*\.blogspot\.com$ 
(^|\.)freeiqtest.*\.blogspot\.com$ 
(^|\.)nauczycielkasex.*\.blogspot\.com$ 
(^|\.)odzyskajsnapy.*\.blogspot\.com$ 
(^|\.)polkawlesie.*\.blogspot\.com$ 
(^|\.)quality-products.*\.blogspot\.com$ 
(^|\.)sexdzieci.*\.blogspot\.com$ 
(^|\.)sexfabryka.*\.blogspot\.com$ 
(^|\.)sex-fabryka.*\.blogspot\.com$ 
(^|\.)szokgwalt.*\.blogspot\.com$ 
(^|\.)twoj-test-iq.*\.blogspot\.com$ 
(^|\.)wideo-pryszcz.*\.blogspot\.com$ 
(^|\.)wyciskaniepryszcza0.*\.blogspot\.com$ 
(^|\.)wyciskanie-pryszcza.*\.blogspot\.com$ 
(^|\.)wygrajnike.*\.blogspot\.com$ 
(^|\.)zgarnijnike.*\.blogspot\.com$ 
(^|\.)zmienkolory.*\.blogspot\.com$ 
(^|\.).*fejs-colors\.blogspot\.com$ 
(^|\.).*internet-lte-5gb\.blogspot\.com$ 
(^|\.).*pakiety-lte-5gb\.blogspot\.com$ 
(^|\.).*pakiety-pokemon-go\.blogspot\.com$ 
(.*)\.g0[0-9]\.(.*) 
^.+\.(love|luxe|realestate)$ 
((^)|(.))adchoice. 
((^)|(.))ads.roku.com 
((^)|(.))adsdk. 
((^)|(.))adserv. 
((^)|(.))analytic. 
((^)|(.))logs.roku.com 
((^)|(.))metric. 
((^)|(.))telemetry. 
(.*)\.g00\.(.*) 
(^|\.)xn--.*$ 
(ads|captive|cloudservices|logs).roku.com$ 
.*\.g00\..* 
^ad([sxv]?[0-9]*|system)[-_.]([^.[:space:]]+\.){1,}|[-_.]ad([sxv]?[0-9]*|system)[-_.] 
^(.+[-_.])?adse?rv(er?|ice)?s?[0-9]*[-_.] 
^(.+[-_.])?telemetry[-_.] 
^adtrack(er|ing)?[0-9]*[-_.] 
^aff(iliat(es?|ion))?[-_.] 
^analytics?[-_.] 
^banners?[-_.] 
^beacons?[0-9]*[-_.] 
^count(ers?)?[0-9]*[-_.] 
^pixels?[-_.] 
(^|\.)abogado$ 
(^|\.)ac$ 
(^|\.)academy$ 
(^|\.)accountant$ 
(^|\.)accountants$ 
(^|\.)actor$ 
(^|\.)ads$ 
(^|\.)adult$ 
(^|\.)africa$ 
(^|\.)ag$ 
(^|\.)agency$ 
(^|\.)airforce$ 
(^|\.)alsace$ 
(^|\.)am$ 
(^|\.)amsterdam$ 
(^|\.)analytics$ 
(^|\.)apartments$ 
(^|\.)arab$ 
(^|\.)archi$ 
(^|\.)art$ 
(^|\.)asia$ 
(^|\.)associates$ 
(^|\.)attorney$ 
(^|\.)auction$ 
(^|\.)audio$ 
(^|\.)author$ 
(^|\.)auto$ 
(^|\.)autos$ 
(^|\.)baby$ 
(^|\.)band$ 
(^|\.)bank$ 
(^|\.)bar$ 
(^|\.)barcelona$ 
(^|\.)bargains$ 
(^|\.)baseball$ 
(^|\.)basketball$ 
(^|\.)bayern$ 
(^|\.)bcn$ 
(^|\.)beauty$ 
(^|\.)beer$ 
(^|\.)best$ 
(^|\.)bet$ 
(^|\.)bible$ 
(^|\.)bid$ 
(^|\.)bike$ 
(^|\.)bingo$ 
(^|\.)bio$ 
(^|\.)biz$ 
(^|\.)black$ 
(^|\.)blackfriday$ 
(^|\.)blue$ 
(^|\.)boats$ 
(^|\.)boo$ 
(^|\.)book$ 
(^|\.)boston$ 
(^|\.)bot$ 
(^|\.)boutique$ 
(^|\.)br.com$ 
(^|\.)broadway$ 
(^|\.)broker$ 
(^|\.)brussels$ 
(^|\.)budapest$ 
(^|\.)build$ 
(^|\.)builders$ 
(^|\.)business$ 
(^|\.)buy$ 
(^|\.)buzz$ 
(^|\.)bz$ 
(^|\.)bzh$ 
(^|\.)cab$ 
(^|\.)call$ 
(^|\.)cam$ 
(^|\.)camera$ 
(^|\.)camp$ 
(^|\.)cancerresearch$ 
(^|\.)capetown$ 
(^|\.)capital$ 
(^|\.)car$ 
(^|\.)cards$ 
(^|\.)care$ 
(^|\.)career$ 
(^|\.)cars$ 
(^|\.)casa$ 
(^|\.)cash$ 
(^|\.)casino$ 
(^|\.)cat$ 
(^|\.)catering$ 
(^|\.)catholic$ 
(^|\.)center$ 
(^|\.)ceo$ 
(^|\.)channel$ 
(^|\.)charity$ 
(^|\.)cheap$ 
(^|\.)christmas$ 
(^|\.)church$ 
(^|\.)city$ 
(^|\.)claims$ 
(^|\.)cleaning$ 
(^|\.)click$ 
(^|\.)clinic$ 
(^|\.)clothing$ 
(^|\.)club$ 
(^|\.)cm$ 
(^|\.)cn.com$ 
(^|\.)co.com$ 
(^|\.)co.in$ 
(^|\.)coach$ 
(^|\.)codes$ 
(^|\.)cologne$ 
(^|\.)com.cn$ 
(^|\.)com.co$ 
(^|\.)com.mx$ 
(^|\.)company$ 
(^|\.)computer$ 
(^|\.)comsec$ 
(^|\.)condos$ 
(^|\.)construction$ 
(^|\.)consulting$ 
(^|\.)contact$ 
(^|\.)contractors$ 
(^|\.)cooking$ 
(^|\.)cool$ 
(^|\.)corn$ 
(^|\.)corp$ 
(^|\.)country$ 
(^|\.)coupon$ 
(^|\.)coupons$ 
(^|\.)courses$ 
(^|\.)cpa$ 
(^|\.)credit$ 
(^|\.)creditcard$ 
(^|\.)creditunion$ 
(^|\.)cricket$ 
(^|\.)cruise$ 
(^|\.)cruises$ 
(^|\.)cymru$ 
(^|\.)dad$ 
(^|\.)dance$ 
(^|\.)data$ 
(^|\.)dating$ 
(^|\.)day$ 
(^|\.)dds$ 
(^|\.)de.com$ 
(^|\.)deal$ 
(^|\.)deals$ 
(^|\.)degree$ 
(^|\.)delivery$ 
(^|\.)democrat$ 
(^|\.)dental$ 
(^|\.)dentist$ 
(^|\.)desi$ 
(^|\.)diamonds$ 
(^|\.)diet$ 
(^|\.)digital$ 
(^|\.)direct$ 
(^|\.)directory$ 
(^|\.)discount$ 
(^|\.)diy$ 
(^|\.)docs$ 
(^|\.)doctor$ 
(^|\.)dog$ 
(^|\.)domains$ 
(^|\.)dot$ 
(^|\.)download$ 
(^|\.)dubai$ 
(^|\.)durban$ 
(^|\.)dvr$ 
(^|\.)earth$ 
(^|\.)eat$ 
(^|\.)eco$ 
(^|\.)ecom$ 
(^|\.)education$ 
(^|\.)energy$ 
(^|\.)engineer$ 
(^|\.)engineering$ 
(^|\.)enterprises$ 
(^|\.)equipment$ 
(^|\.)esq$ 
(^|\.)estate$ 
(^|\.)eu.com$ 
(^|\.)events$ 
(^|\.)exchange$ 
(^|\.)expert$ 
(^|\.)exposed$ 
(^|\.)express$ 
(^|\.)fail$ 
(^|\.)faith$ 
(^|\.)family$ 
(^|\.)fan$ 
(^|\.)fans$ 
(^|\.)farm$ 
(^|\.)fashion$ 
(^|\.)feedback$ 
(^|\.)film$ 
(^|\.)final$ 
(^|\.)finance$ 
(^|\.)financial$ 
(^|\.)finish$ 
(^|\.)firm.in$ 
(^|\.)fish$ 
(^|\.)fishing$ 
(^|\.)fit$ 
(^|\.)fitness$ 
(^|\.)flights$ 
(^|\.)florist$ 
(^|\.)flowers$ 
(^|\.)fly$ 
(^|\.)foo$ 
(^|\.)food$ 
(^|\.)football$ 
(^|\.)forsale$ 
(^|\.)forum$ 
(^|\.)frl$ 
(^|\.)furniture$ 
(^|\.)futbol$ 
(^|\.)fyi$ 
(^|\.)ga$ 
(^|\.)gal$ 
(^|\.)gallery$ 
(^|\.)game$ 
(^|\.)games$ 
(^|\.)garden$ 
(^|\.)gay$ 
(^|\.)ged$ 
(^|\.)gen.in$ 
(^|\.)gent$ 
(^|\.)gdn$ 
(^|\.)gift$ 
(^|\.)gifts$ 
(^|\.)gives$ 
(^|\.)giving$ 
(^|\.)global$ 
(^|\.)gmbh$ 
(^|\.)gold$ 
(^|\.)golf$ 
(^|\.)got$ 
(^|\.)gq$ 
(^|\.)gr.com$ 
(^|\.)graphics$ 
(^|\.)gratis$ 
(^|\.)green$ 
(^|\.)gripe$ 
(^|\.)grocery$ 
(^|\.)gs$ 
(^|\.)guide$ 
(^|\.)guitars$ 
(^|\.)gy$ 
(^|\.)hair$ 
(^|\.)halal$ 
(^|\.)hamburg$ 
(^|\.)haus$ 
(^|\.)health$ 
(^|\.)healthcare$ 
(^|\.)helsinki$ 
(^|\.)here$ 
(^|\.)hiphop$ 
(^|\.)hiv$ 
(^|\.)hn$ 
(^|\.)hockey$ 
(^|\.)holdings$ 
(^|\.)holiday$ 
(^|\.)home$ 
(^|\.)homes$ 
(^|\.)horse$ 
(^|\.)hospital$ 
(^|\.)host$ 
(^|\.)hosting$ 
(^|\.)hot$ 
(^|\.)hoteis$ 
(^|\.)hotel$ 
(^|\.)hotels$ 
(^|\.)house$ 
(^|\.)how$ 
(^|\.)idn$ 
(^|\.)immo$ 
(^|\.)immobilien$ 
(^|\.)ind.in$ 
(^|\.)industries$ 
(^|\.)ing$ 
(^|\.)ink$ 
(^|\.)institute$ 
(^|\.)insurance$ 
(^|\.)insure$ 
(^|\.)international$ 
(^|\.)investments$ 
(^|\.)irish$ 
(^|\.)islam$ 
(^|\.)ismaili$ 
(^|\.)ist$ 
(^|\.)istanbul$ 
(^|\.)jetzt$ 
(^|\.)jewelry$ 
(^|\.)jobs$ 
(^|\.)joburg$ 
(^|\.)jot$ 
(^|\.)joy$ 
(^|\.)jpn.com$ 
(^|\.)juegos$ 
(^|\.)kaufen$ 
(^|\.)ki$ 
(^|\.)kid$ 
(^|\.)kids$ 
(^|\.)kim$ 
(^|\.)kitchen$ 
(^|\.)kiwi$ 
(^|\.)kosher$ 
(^|\.)kyoto$ 
(^|\.)la$ 
(^|\.)land$ 
(^|\.)lat$ 
(^|\.)latino$ 
(^|\.)lawyer$ 
(^|\.)lc$ 
(^|\.)lease$ 
(^|\.)legal$ 
(^|\.)lifeinsurance$ 
(^|\.)lifestyle$ 
(^|\.)lighting$ 
(^|\.)like$ 
(^|\.)limited$ 
(^|\.)limo$ 
(^|\.)living$ 
(^|\.)llc$ 
(^|\.)llp$ 
(^|\.)loan$ 
(^|\.)loans$ 
(^|\.)london$ 
(^|\.)love$ 
(^|\.)ltd$ 
(^|\.)ltda$ 
(^|\.)luxe$ 
(^|\.)luxury$ 
(^|\.)madrid$ 
(^|\.)mail$ 
(^|\.)maison$ 
(^|\.)makeup$ 
(^|\.)management$ 
(^|\.)map$ 
(^|\.)market$ 
(^|\.)marketing$ 
(^|\.)mba$ 
(^|\.)me.uk$ 
(^|\.)med$ 
(^|\.)medical$ 
(^|\.)meet$ 
(^|\.)melbourne$ 
(^|\.)meme$ 
(^|\.)memorial$ 
(^|\.)men$ 
(^|\.)menu$ 
(^|\.)miami$ 
(^|\.)mls$ 
(^|\.)mn$ 
(^|\.)mobi$ 
(^|\.)mobile$ 
(^|\.)moda$ 
(^|\.)moe$ 
(^|\.)mom$ 
(^|\.)money$ 
(^|\.)mortgage$ 
(^|\.)moscow$ 
(^|\.)moto$ 
(^|\.)motorcycles$ 
(^|\.)mov$ 
(^|\.)movie$ 
(^|\.)music$ 
(^|\.)mutualfunds$ 
(^|\.)mx$ 
(^|\.)nagoya$ 
(^|\.)navy$ 
(^|\.)net.cn$ 
(^|\.)net.in$ 
(^|\.)net.nz$ 
(^|\.)new$ 
(^|\.)nf$ 
(^|\.)now$ 
(^|\.)nyc$ 
(^|\.)okinawa$ 
(^|\.)onl$ 
(^|\.)ooo$ 
(^|\.)org.cn$ 
(^|\.)org.in$ 
(^|\.)org.nz$ 
(^|\.)organic$ 
(^|\.)osaka$ 
(^|\.)ott$ 
(^|\.)paris$ 
(^|\.)pars$ 
(^|\.)partners$ 
(^|\.)parts$ 
(^|\.)party$ 
(^|\.)pay$ 
(^|\.)pets$ 
(^|\.)phd$ 
(^|\.)phone$ 
(^|\.)photo$ 
(^|\.)photography$ 
(^|\.)photos$ 
(^|\.)physio$ 
(^|\.)pics$ 
(^|\.)pictures$ 
(^|\.)pid$ 
(^|\.)ping$ 
(^|\.)pink$ 
(^|\.)pizza$ 
(^|\.)place$ 
(^|\.)play$ 
(^|\.)plumbing$ 
(^|\.)pm$ 
(^|\.)poker$ 
(^|\.)politie$ 
(^|\.)press$ 
(^|\.)productions$ 
(^|\.)prof$ 
(^|\.)promo$ 
(^|\.)properties$ 
(^|\.)property$ 
(^|\.)protection$ 
(^|\.)qpon$ 
(^|\.)quebec$ 
(^|\.)racing$ 
(^|\.)radio$ 
(^|\.)radio.am$ 
(^|\.)radio.fm$ 
(^|\.)re$ 
(^|\.)read$ 
(^|\.)realestate$ 
(^|\.)realtor$ 
(^|\.)realty$ 
(^|\.)recipes$ 
(^|\.)red$ 
(^|\.)rehab$ 
(^|\.)reise$ 
(^|\.)reisen$ 
(^|\.)rent$ 
(^|\.)rentals$ 
(^|\.)repair$ 
(^|\.)report$ 
(^|\.)republican$ 
(^|\.)rest$ 
(^|\.)restaurant$ 
(^|\.)retirement$ 
(^|\.)review$ 
(^|\.)reviews$ 
(^|\.)rich$ 
(^|\.)rodeo$ 
(^|\.)roma$ 
(^|\.)room$ 
(^|\.)rsvp$ 
(^|\.)ru.com$ 
(^|\.)rugby$ 
(^|\.)ruhr$ 
(^|\.)run$ 
(^|\.)ryukyu$ 
(^|\.)sa.com$ 
(^|\.)saarland$ 
(^|\.)safe$ 
(^|\.)safety$ 
(^|\.)sale$ 
(^|\.)salon$ 
(^|\.)sari$ 
(^|\.)sarl$ 
(^|\.)save$ 
(^|\.)sb$ 
(^|\.)sc$ 
(^|\.)scholarships$ 
(^|\.)school$ 
(^|\.)scot$ 
(^|\.)se.net$ 
(^|\.)sex$ 
(^|\.)sexy$ 
(^|\.)shabaka$ 
(^|\.)shabaka-arabic$ 
(^|\.)shia$ 
(^|\.)shiksha$ 
(^|\.)show$ 
(^|\.)singles$ 
(^|\.)site$ 
(^|\.)ski$ 
(^|\.)smile$ 
(^|\.)so$ 
(^|\.)soccer$ 
(^|\.)solar$ 
(^|\.)solutions$ 
(^|\.)song$ 
(^|\.)soy$ 
(^|\.)spa$ 
(^|\.)sport$ 
(^|\.)sports$ 
(^|\.)spot$ 
(^|\.)srl$ 
(^|\.)stockholm$ 
(^|\.)storage$ 
(^|\.)stream$ 
(^|\.)studio$ 
(^|\.)study$ 
(^|\.)style$ 
(^|\.)sucks$ 
(^|\.)supplies$ 
(^|\.)supply$ 
(^|\.)support$ 
(^|\.)surgery$ 
(^|\.)swiss$ 
(^|\.)sydney$ 
(^|\.)taipei$ 
(^|\.)talk$ 
(^|\.)tatar$ 
(^|\.)tattoo$ 
(^|\.)tax$ 
(^|\.)taxi$ 
(^|\.)team$ 
(^|\.)tel$ 
(^|\.)tennis$ 
(^|\.)tf$ 
(^|\.)thai$ 
(^|\.)theater$ 
(^|\.)theatre$ 
(^|\.)tickets$ 
(^|\.)tienda$ 
(^|\.)tires$ 
(^|\.)tirol$ 
(^|\.)tl$ 
(^|\.)today$ 
(^|\.)tokyo$ 
(^|\.)tours$ 
(^|\.)town$ 
(^|\.)toys$ 
(^|\.)trade$ 
(^|\.)trading$ 
(^|\.)training$ 
(^|\.)travel$ 
(^|\.)tube$ 
(^|\.)uk.com$ 
(^|\.)uk.net$ 
(^|\.)university$ 
(^|\.)uno$ 
(^|\.)vacations$ 
(^|\.)vegas$ 
(^|\.)ventures$ 
(^|\.)versicherung$ 
(^|\.)vet$ 
(^|\.)viajes$ 
(^|\.)villas$ 
(^|\.)vin$ 
(^|\.)vip$ 
(^|\.)vision$ 
(^|\.)vlaanderen$ 
(^|\.)vodka$ 
(^|\.)vote$ 
(^|\.)voting$ 
(^|\.)voto$ 
(^|\.)voyage$ 
(^|\.)wales$ 
(^|\.)wang$ 
(^|\.)watch$ 
(^|\.)watches$ 
(^|\.)weather$ 
(^|\.)web$ 
(^|\.)webcam$ 
(^|\.)webs$ 
(^|\.)website$ 
(^|\.)wedding$ 
(^|\.)wf$ 
(^|\.)win$ 
(^|\.)wine$ 
(^|\.)winners$ 
(^|\.)work$ 
(^|\.)wow$ 
(^|\.)ws$ 
(^|\.)xin$ 
(^|\.)xxx$ 
(^|\.)yachts$ 
(^|\.)yoga$ 
(^|\.)yokohama$ 
(^|\.)you$ 
(^|\.)ytt$ 
(^|\.)za.com$ 
(^|\.)zero$ 
(^|\.)zip$ 
(^|\.)coin-hive\.com$ 
(^|\.)coinhive\.com$ 
(^|\.)midgifts\.racing$ 
(^|\.)adexchangecloud\.com$ 
(^|\.)inclk\.com$ 
(^|\.)gfe\.nvidia\.com$ 
(^|\.)kergaukr\.com$ 
(^|\.)c11ff582fa2fd7dc\.com$ 
(^|\.)chatango\.com$ 
(^|\.)searchtuner\.com$ 
(^|\.)whois\.co\.kr$ 
(^|\.)revenuehits\.com$ 
(^|\.)yesadvertising\.com$ 
(^|\.)adsterra\.com$ 
(^|\.)clicky\.com$ 
(^|\.)gaug\.es$ 
(^|\.)woopra\.com$ 
(^|\.)openwebanalytics\.com$ 
(^|\.)wlzohrpjbuq\.com$ 
(^|\.)sape\.ru$ 
(^|\.)download-geek\.com$ 
(^|\.)sunmaker\.com$ 
(^|\.)mgid\.com$ 
(^|\.)zanox\.com$ 
(^|\.)adk2x\.com$ 
(^|\.)telemetry\.mozilla\.org$ 
(^|\.)freeprotect584\.com$ 
(^|\.)wiserapps\.site$ 
(^|\.)free3dadultgames\.com$ 
(^|\.)ck\.juicyads\.com$ 
(^|\.)juicyads\.com$ 
(^|\.)mediadownload\.site$ 
(^|\.)exosrv\.com$ 
(^|\.)adswizz\.com$ 
(^|\.)bitbounce\.com$ 
(^|\.)nine-our-prize20\.loan$ 
(^|\.)adextrem\.com$ 
(^|\.)awenetwork\.com$ 
(^|\.)etology\.com$ 
(^|\.)buysellads\.net$ 
(^|\.)clinch\.co$ 
(^|\.)ads\.trafficjunky\.net$ 
(^|\.)sunnyplayer\.com$ 
(^|\.)platincasino\.com$ 
(^|\.)exceptionless\.io$ 
(^|\.)cpmnotify\.com$ 
(^|\.)websnewsdate\.com$ 
(^|\.)onlinecasinoreports\.com$ 
(^|\.)cdnondemand\.org$ 
(^|\.)cobalten\.com$ 
(^|\.)news-subscribe\.com$ 
(^|\.)swiftfling\.com$ 
(^|\.)digitalmerkat\.com$ 
(^|\.)xirvecaten\.com$ 
(^|\.)offerzone\.click$ 
(^|\.)wrison-subustall\.com$ 
(^|\.)cpokotex\.com$ 
(^|\.)datingbaron\.com$ 
(^|\.)optmnstr\.com$ 
(^|\.)demobileint\.xyz$ 
(^|\.)blastnotificationx\.com$ 
(^|\.)schneevonmorgen\.com$ 
(^|\.)meetrics\.net$ 
(^|\.)svonm\.com$ 
(^|\.)trafficjunky\.net$ 
(^|\.)ioam\.de$ 
(^|\.)securecloud-smart\.com$ 
(^|\.)hot-offer-for-you\.com$ 
(^|\.)outbrain\.com$ 
(^|\.)outbrainimg\.com$ 
(^|\.)globvill\.de$ 
(^|\.)internetat\.tv$(^|\.)doubleclick\.net$ 
(^|\.)mordi\.fun$ 
(^|\.)watchmygf\.to$ 
(^|\.)rhfgjld\.com$ 
(^|\.)big7\.com$ 
(^|\.)exdynsrv\.com$ 
(^|\.)arminius\.io$ 
(^|\.)comdexcipa\.info$ 
(^|\.)lenovomm\.com$ 
(^|\.)gvt2\.com$ 
(^|\.)pcmdnsrv\.com$ 
(^|\.)bigbughere81\.agency$ 
(^|\.)outbrain\.org$ 
(^|\.)bidhead\.net$ 
(^|\.)bodloster\.com$ 
(^|\.)adtags\.pro$ 
(^|\.)qwerty24\.net$ 
(^|\.)avast\.com$ 
(^|\.)mybetterdl\.com$ 
(^|\.)taboola\.com$ 
(^|\.)hotjar\.com$ 
(^|\.)piwik$ 
(^|\.)adobe\.io$ 
(^|\.)adobelogin\.com$ 
(^|\.)cleverpush\.com$ 
(^|\.)footprintdns\.com$ 
(^|\.)localhookup$ 
(^|\.)meetuptoday$ 
(^|\.)montafp\.top$ 
(^|\.)wpnrtnmrewunrtok\.xyz$ 
(^|\.)govdelivery\.com$ 
(^|\.)caferio\.com$ 
^track(ing)?[0-9]*[_.-] 
(^|\.)messserver\.de$ 
(\.|^)tgbmx\.xyz$ 
(\.|^)contentabc\.com$ 
(\.|^)2020mustang\.com$ 
(\.|^)logsss\.com$ 
(\.|^)izatcloud\.net$ 
(\.|^)trafiq\.bid$ 
(\.|^)sexb\.me$ 
(\.|^)aklamio\.com$ 
(\.|^)track18\.com$ 
(\.|^)clicahy\.com$ 
(\.|^)intl\.xiaomi\.com$ 
^(.+[-_.])??telemetry[-.] 
google-{0,}(analytic|syndication|(ad[a-z0-9]*|tag)-{0,}service)[s]\.[a-z]{2,7}$ 
google-{0,}(analytics{0,}|(ad|tag)manager)\.[a-z]{2,7}$ 
double-{0,}clic(k|k[.]*by-{0,}google)\.[a-z]{2,7}$ 
(google|partner|pub)-{0,}ads{0,}-{0,}(apis{0,})\.[a-z]{2,7}$ 
(^|\.)facebook\.[a-za-z0-9]+$ 
(^|\.)fb\.[a-za-z0-9]+$ 
(^|\.)fbcdn\.[a-za-z0-9]+$ 
(^|\.)fbsbx\.com$ 
(^|\.)fbsbx\.com\.online-metrix\.net$ 
(^|\.)m\.me$ 
(^|\.)messenger\.com$ 
(^|\.)tfbnw\.net$ 
^(.+\.)?cdn\.ampproject\.org$ 
([A-Za-z0-9.-]*\.)?clicks\.beap\.bc\.yahoo\.com/ 
([A-Za-z0-9.-]*\.)?secure\.footprint\.net/ 
([A-Za-z0-9.-]*\.)?match\.com/ 
([A-Za-z0-9.-]*\.)?sitescout(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?appnexus(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?evidon(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?mediamath(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?scorecardresearch(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?doubleclick(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?flashtalking(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?turn(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?mathtag(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?googlesyndication(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?s\.yimg\.com/cv/ae/us/audience/ 
([A-Za-z0-9.-]*\.)?clicks\.beap/ 
([A-Za-z0-9.-]*\.)?.doubleclick(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?yieldmanager(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?w55c(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?adnxs(\.\w{2}\.\w{2}|\.\w{2,4})/ 
([A-Za-z0-9.-]*\.)?advertising\.com/ 
([A-Za-z0-9.-]*\.)?evidon\.com/ 
([A-Za-z0-9.-]*\.)?scorecardresearch\.com/ 
([A-Za-z0-9.-]*\.)?flashtalking\.com/ 
([A-Za-z0-9.-]*\.)?turn\.com/ 
([A-Za-z0-9.-]*\.)?mathtag\.com/ 
([A-Za-z0-9.-]*\.)?surveylink/ 
([A-Za-z0-9.-]*\.)?info\.yahoo\.com/ 
([A-Za-z0-9.-]*\.)?ads\.yahoo\.com/ 
([A-Za-z0-9.-]*\.)?global\.ard\.yahoo\.com/ 
(^|\.)lgsmartad\.com$ 
(^|\.)buffpanel\.com$ 
(^|\.)bugsnag\.com$ 
(^|\.)redshell\.io$ 
(^|\.)treasuredata\.com$ 
(^|\.)unity(|3d)\.com$ 
(^|\.)unityads(|\.co)\.com$ 
watson\..*\.Microsoft\.com 
^wpad\. 
(\.cn$|\.ru$|\.su$|\.vn$|\.top$) 
sendgrid\.net$ 
.*(xn--).* 
(\.porn$|\.sex$|\.xxx$|\.sexy$|\.webcam$|\.cam$|\.tube$|\.adult$|\.gay$) 
watson\..*\.microsoft\.com 
(\.casino$|\.bet$|\.poker$) 
/ad([sxv]?[0-9]*|system)[_.-]([^.[:space:]]+\.){1,}|[_.-]ad([sxv]?[0-9]*|system)[_.-]/ 
/(.+[_.-])?adse?rv(er?|ice)?s?[0-9]*[_.-]/ 
/(.+[_.-])?telemetry[_.-]/ 
/adim(age|g)s?[0-9]*[_.-]/ 
/^adtrack(er|ing)?[0-9]*[_.-]/ 
/^advert(s|is(ing|ements?))?[0-9]*[_.-]/ 
/aff(iliat(es?|ion))?[_.-]/ 
/analytics?[_.-]/ 
/banners?[_.-]/ 
/beacons?[0-9]*[_.-]/ 
/count(ers?)?[0-9]*[_.-]/ 
/mads\./ 
/pixels?[-.]/ 
/stat(s|istics)?[0-9]*[_.-]/``` 
(x[n]?xx|xvideo|porn|sex).*\. 
/.zip$ 
\.[a-z][0-9]{4}\.com$ 
^hy[0-9]{2,4}.com$ 